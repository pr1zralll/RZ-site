﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using WebApplication3.Models;
using MySQL.Data.EntityFrameworkCore.Extensions;

namespace WebApplication3.Context
{
    public class NewsContext : DbContext
    {
        public DbSet<News> news { get; set; }
        public DbSet<Article> article { get; set; }
        public DbSet<Pages> pages { get; set; }
        public DbSet<ArticleImages> articleimages { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=;UserId=root;Password=;database=mydb;SslMode=none;");
        }
        
    }

}
