using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class ImagesController : Controller
    {
        public ActionResult GetById(int id)
        {
            try
            {
                return new FileStreamResult(ArticleImages.getByID(id), "image/jpeg");
            }
            catch (Exception) { return null; }
        }
    }
}