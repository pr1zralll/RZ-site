using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication3.Context;
using WebApplication3.Models;
using System.IO;

namespace WebApplication3.Controllers
{
    public class ArticlesController : Controller
    {
       
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Create([Bind("id,tittle,date,time,info,full,file")] ArticleModel art)
        {
            if (ModelState.IsValid && art.file != null)
            {
                byte[] file = null;
                using (var binaryReader = new BinaryReader(art.file.OpenReadStream()))
                {
                    file = binaryReader.ReadBytes((int)art.file.Length);
                }
                using (NewsContext _context = new NewsContext())
                {
                    Article article = new Article() { id=art.id, date = art.date, tittle = art.tittle, info = art.info.Substring(0, 195) + "...", time = art.time};
                    ArticleImages image = new ArticleImages() { id=art.id, name=art.file.FileName, img = file};
                    Pages page = new Pages() { id=art.id, Full=art.full};
                    


                    _context.article.Add(article);
                    _context.articleimages.Add(image);
                    _context.pages.Add(page);

                    _context.SaveChanges();
                    return Content("ok");
                }
            }
            return View(art);
        }

        public IActionResult Delete(int id)
        {
            using (NewsContext _context = new NewsContext())
            {
                const string query = "DELETE FROM mydb.article WHERE id = {0}";
                const string query2 = "DELETE FROM mydb.pages WHERE id = {0}";
                const string query3 = "DELETE FROM mydb.articleimages WHERE id = {0}";
                _context.Database.ExecuteSqlCommand(query, id);
                _context.Database.ExecuteSqlCommand(query2, id);
                _context.Database.ExecuteSqlCommand(query3, id);
            }
                return Content("ok");
        }
    }

}
