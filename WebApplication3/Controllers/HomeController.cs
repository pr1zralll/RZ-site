﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.Context;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Archive(int page = 1)
        {
            Init();
            ViewData["id"] = page;
            return View(News.getNews(page));
        }

        public IActionResult Articles(int page = 1)
        {
            Init();
            ViewData["id"] = page;
            return View(Models.Article.getArts(page));
        }

        public IActionResult Article(int id)
        {
            Init();
            Article article = Models.Article.getById(id);
            if (article == null)
                return RedirectToAction("HttpNotFound");
            return View(article);
        }

        public IActionResult HttpNotFound()
        {
            return View();
        }

        public IActionResult Date(DateTime date)
        {
            Init();
            return View(News.getByDate(date));
        }

        public IActionResult Index()
        {
            Init();
            ViewData["state"] = Models.Article.getLast();
            return View(News.getLastNews(9));
        }

        public IActionResult Rules()
        {
            Init();
            return View();
        }
       
        public IActionResult Ads()
        {
            Init();
            return View();
        }

        public IActionResult View(int id)
        {
            Init();
            return View(News.getById(id));
        }

        public IActionResult Contact()
        {
            Init();
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        private void InitLastNews()
        {
            ViewData["last"] = News.getLastNews(6);
        }

        private void InitCounter()
        {
            try
            {
                if (sr == null)
                    sr = new StreamReader(fs);
                String line = sr.ReadToEnd();

                long c01 = 0;
                long c02 = 0;

                if (line.Length > 0)
                {
                    var lines = line.Split(',');
                    c01 = Int64.Parse(lines[0]) + 1;
                    c02 = Int64.Parse(lines[1]) + 1;
                }
                else
                {
                    c01 = 29253;
                    c02 = 188156;
                }

                ViewData["c01"] = c01;
                ViewData["c02"] = c02;

                if (sw == null)
                    sw = new StreamWriter(fs);
                fs.Seek(0, SeekOrigin.Begin);
                sw.Write(c01 + "," + c02);
                sw.FlushAsync();
            }
            catch (Exception)
            {
                ViewData["c01"] = -1;
                ViewData["c02"] = -1;
            }
        }

        private StreamReader sr = null;
        private StreamWriter sw = null;

        private FileStream fs = new FileStream(Directory.GetCurrentDirectory() + "\\parameters", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);

        private void Init()
        {
            InitCounter();
            InitLastNews();
        }
    }
}