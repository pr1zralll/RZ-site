﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using WebApplication3.Context;

namespace WebApplication3.Models
{
    public class ArticleImages
    {
        public int id{ get; set; }
        public string name { get; set; }
        public byte[] img { get; set; }

        public static Stream getByID(int id)
        {
            try
            {
                var bytes = new NewsContext().articleimages.First(x => x.id == id).img;
                var steam = new MemoryStream(bytes);
                return steam;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
