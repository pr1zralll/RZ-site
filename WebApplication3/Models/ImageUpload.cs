﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class ImageUpload
    {
        public int id { get; set; }
        public IFormFile file { get; set; }
    }
}
