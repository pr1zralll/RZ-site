﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.Context;

namespace WebApplication3.Models
{
    public class Article
    {
        public int id { get; set; }
        public String tittle { get; set; }

        public DateTime date
        {
            get;
            set;
        }

        public TimeSpan time { get; set; }
        public String info { get; set; }


        public String getFull()
        {
            return new NewsContext().pages.First(x=>x.id==id).Full;
        }

        public string getDate()
        {
            return date.ToString("dd MMMM yyyy");
        }
        public static List<Article> getTempArts(int count)
        {
            var list = new List<Article>();
            for (var i = 0; i < count; i++)
                list.Add(new Article
                {
                    id = i,
                    date = DateTime.MinValue,
                    info = "info info info info info info info info",
                    tittle = "tittle tittle",
                    time = TimeSpan.MinValue
                });
            return list;
        }

        public static Article getLast()
        {
            try
            {
                var db = new NewsContext();
                return db.article.OrderByDescending(x => x.date).ThenByDescending(x => x.time).First();
            }
            catch (Exception)
            {
                return getTempArts(1)[0];
            }
        }

        public static Article getById(int id)
        {
            try
            {
                var db = new NewsContext();
                return db.article.First(x => x.id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Article> getArts(int page)
        {
            try
            {
                NewsContext db = new NewsContext();
                int skip = 6 * (page - 1);
                return db.article.OrderByDescending(x => x.date).ThenByDescending(x => x.time).Skip(skip).Take(6).ToList();
            }
            catch (Exception e)
            {
                return getTempArts(6);
            }
        }

        public object getName()
        {
            throw new NotImplementedException();
        }
    }
}
