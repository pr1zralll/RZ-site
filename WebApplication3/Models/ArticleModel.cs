﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class ArticleModel
    {
        public int id { get; set; }
        public String tittle { get; set; }
        public DateTime date { get; set; }
        public TimeSpan time { get; set; }
        public String info { get; set; }
        public string full { get; set; }
        public IFormFile file { get; set; }
    }
}
