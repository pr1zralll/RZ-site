﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using WebApplication3.Context;

namespace WebApplication3.Models
{
    public class News
    {
        public int Id { get; set; }
        public String Tittle { get; set; }
        public DateTime Date { get; set; }
        public String Info { get; set; }
        public byte[] Img { get; set; }
        public String Full { get; set; }

        public static List<News> getTempNews(int count)
        {
            var list = new List<News>();
            for (var i = 0; i < count; i++)
                list.Add(new News
                {
                    Id = i,
                    Date = DateTime.MinValue,
                    Full = "full full full full full full full full full full full full full full full full full full full full full full full full full full full full full full full full full full full",
                    Img = System.IO.File.ReadAllBytes(@"wwwroot/images/test.png"),
                    Info = "info info info info info info info info",
                    Tittle = "tittle tittle tittle tittle tittle tittle tittle tittle tittle tittle"
                });
            return list;
        }

        public static List<News> getLastNews(int count)
        {
            try
            {
                var db = new NewsContext();
                return db.news.OrderByDescending(x => x.Date).Take(count).ToList();
            }
            catch (Exception)
            {
                return News.getTempNews(count);
            }
        }

        public static News getById(int id)
        {
            try
            {
                var db = new NewsContext();
                return db.news.First(i => i.Id == id);
            }
            catch (Exception )
            {
                return getTempNews(1)[0];
            }
        }

        public static List<News> getByDate(DateTime date)
        {
            try
            {
                NewsContext db = new NewsContext();
                return db.news.Where(x => x.Date > date && x.Date < date.AddDays(1)).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<News> getNews(int page)
        {
            try
            {
                NewsContext db = new NewsContext();
                return db.news.OrderByDescending(x => x.Date).Skip((page - 1) * 6).Take(6).ToList();
            }
            catch (Exception )
            {
                return getTempNews(6);
            }
        }
    }
}
